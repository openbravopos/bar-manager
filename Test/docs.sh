#!/bin/sh
echo "Making ReStructredText"
for l in `ls *.rst`; do
	name=`echo $l | cut -d"." -f1`
	rst2html.py $l > ${name}.html
	echo "Written ${name}.html"
done
