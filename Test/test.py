""" Gabbi Test example runner
"""
import os
from gabbi import driver
import fixtures
# Settings
TESTS_DIR = 'sample'
host = "192.168.0.5"
port = "80"
prefix = "Projects/bar-manager/index.php"

def load_tests(loader, tests, pattern):
    """ Search for tests
    """
    test_dir = os.path.join(os.path.dirname(__file__), TESTS_DIR)
    return driver.build_tests(test_dir, loader,
                              host=host,
                              port=port,
                              prefix=prefix,
                              fixture_module=fixtures)