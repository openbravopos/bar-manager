"""Test Loader
"""

import os
import sys
from gabbi import driver

TESTS_DIR = 'sample'

def load_tests(loader, tests, pattern):
    """Provide a TestSuite to the discovery process."""
    
    test_dir = os.path.join(os.path.dirname(__file__), TESTS_DIR)
    return driver.build_tests(test_dir, loader,
                              host="localhost"
                              )

if __name__ == "__main__":
    import unittest
    unittest.main()