"""Test Loader
python -m unittest -v loader
"""

import os
import sys
from gabbi import driver

import fixtures
import ConfigParser
config = ConfigParser.ConfigParser()
config.read('settings.ini') # Load the settings.ini file

TESTS_DIR = 'dev'

default = config.get('Tests','default')
data = dict(config.items(default))

#import cookielib, urllib, urllib2

def load_tests(loader, tests, pattern):
    """Provide a TestSuite to the discovery process."""
    
    test_dir = os.path.join(os.path.dirname(__file__), TESTS_DIR)
    return driver.build_tests(test_dir, loader,
                              host=data['host'],
                              port=data['port'],
                              prefix=data['prefix'],
                              fixture_module=fixtures)

if __name__ == "__main__":
    import unittest
    unittest.main()