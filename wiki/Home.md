# MyPOS

MyPOS is a PHP web application for Unicenta and Chromis POS solutions, it aims to simplify and extend the functionality without changing the software clients, or adversly altering your database.

## Getting Started


## Installation



## Development

MyPOS is written in PHP and uses the CodeIgniter Framework, 

### Testing

Gabbi HTTP Tests are used to run functional tests of the application.
Any new features should introduce a new set of functional tests, and verify existing functionality hasn't regressed.

#### Automated Testing

Docker is configured to deploy automated testing.

[Testing](testing)
[Testing Gabbi](testing/gabbi)