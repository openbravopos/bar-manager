<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Model {
    /* CodeIgniter Model
    Database: Products

    */

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function index($criteria=Null)
    {
        return $this->_list($criteria)->result();
    }
    function exists($value)
    {
        return ($this->_list()->num_rows()==1);
    }
    function _list($criteria=Null)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PRODUCTS.ID as id, PRODUCTS.CODE as code, PRODUCTS.NAME as name, PRODUCTS.PRICEBUY as buyprice, PRODUCTS.PRICESELL as sellprice, PRODUCTS.TAXCAT, PRODUCTS.DISPLAY as label, PRODUCTS_CAT.PRODUCT as cat');
        if(!is_null($criteria)) $this->db->where($criteria);
        $this->db->from('PRODUCTS');
        $this->db->order_by('code');
        $this->db->join('PRODUCTS_CAT','PRODUCTS.ID = PRODUCTS_CAT.PRODUCT','left');
        //$this->db->join('roles','roles.id = people.Role','left');
        return $this->db->get();
    }
    function item($code)
    {
        $this->db->select('PRODUCTS.ID as id, PRODUCTS.CODE as code, PRODUCTS.NAME as name, PRODUCTS.PRICEBUY as buyprice, PRODUCTS.PRICESELL as sellprice, PRODUCTS.TAXCAT, PRODUCTS.DISPLAY as label');
        $this->db->from('PRODUCTS');
        //$this->db->join('roles','roles.id = people.Role','left');
        $this->db->where('PRODUCTS.code = "'.$code.'"');
        return $this->db->get()->first_row();
    }
    function add()
    {

    }
    function show($status)
    {
$this->db->set('field', 'field+1');
$this->db->where('id', 2);
$this->db->update('mytable');
    }
    function delete()
    {

    }
    function create()
    {
        $catagories = array(
                        'CatagoryID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'CatagoryName' => array(
                                                 'type' =>'VARCHAR',
                                                 'constraint' => '100'
                                          ),
                );
        $products = array(
                        'ProductID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'CatagoryID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'ProductName' => array(
                                                 'type' =>'VARCHAR',
                                                 'constraint' => '100',
                                                 'default' => 'King of Town',
                                          ),
                );
        $stock = array(
                        'StockID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'CatagoryID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'ProductName' => array(
                                                 'type' =>'VARCHAR',
                                                 'constraint' => '100',
                                                 'default' => 'King of Town',
                                          ),
                );
        $stocklevels = array(
                        'ID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'StockID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'MinQty' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '3',
                                                 'default' => 'King of Town',
                                          ),
                        'MaxQty' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '3',
                                                 'unsigned' => TRUE
                                          ),
                );
        $linechecks = array(
                        'LineCheckID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'ByID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'DateTime' => array(
                                                 'type' => 'datetime',
                                                 'unsigned' => TRUE
                                          ),
                );

        $linecheckvaluess = array(
                        'ID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'LineCheckID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'ProductID' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'Qty' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),

                );
                $this->dbforge->add_field($catagories);
                $this->dbforge->add_key('CatagoryID');
                $this->dbforge->create_table('CATAGORY');

                $this->dbforge->add_field($products);
                $this->dbforge->add_key('ProductID');
                $this->dbforge->create_table('productlines');

                $this->dbforge->add_field($products);
                $this->dbforge->add_key('StockID');
                $this->dbforge->create_table('stocklines');

                $this->dbforge->add_field($stocklevels);
                $this->dbforge->add_key('ID');
                $this->dbforge->create_table('stocklevels');

                $this->dbforge->add_field($productlinechecks);
                $this->dbforge->add_key('LineCheckID');
                $this->dbforge->create_table('linechecks');

                $this->dbforge->add_field($linecheckvaluess);
                $this->dbforge->add_key('ID');
                $this->dbforge->create_table('linecheckvaluess');
    }

}
?>