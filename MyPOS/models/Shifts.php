<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Shifts extends CI_Model {
    /* CodeIgniter Model
    Database: Shifts
*/
    
    var $STARTSHIFT = 0;
    var $ENDSHIFT = 0;
    var $PPLID = 0;

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function _read($end)
    {
        // List all functions
        $this->db->select('PEOPLE.ID as PersonID, PEOPLE.Name as name, ROLES.Name as role, SHIFTS.STARTSHIFT as start, SHIFTS.ENDSHIFT as end');
        $this->db->from('SHIFTS');
        $this->db->join('PEOPLE', 'PEOPLE.ID = SHIFTS.PPLID', 'left');
        $this->db->join('ROLES', 'ROLES.id = PEOPLE.Role', 'left');

    }
    public function get($end = true)
    {
        $this->_read($end)
        if($end)
            $this->db->where('SHIFTS.ENDSHIFT IS NOT NULL', null);
        $this->db->order_by("start", "desc"); 
        $this->db->order_by("end", "desc"); 
        $data = $this->db->get();
        return $data->result();
    }
    public function detail($val)
    {
    	$this->_read($end)
    	$this->db->where('SHIFTS.ID', $val);
        $data = $this->db->get();
        return $data->result();
    }
    public function create()
    {

    }
    public function update($val)
    {
        $this->db->where('SHIFTS.ID', $val);
    }
    public function delete($val)
    {
        $this->db->where('SHIFTS.ID', $val);
    }

}
?>