<?php
/*
Manage the users of the application
*/
class Users extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->dbforge();
    }
    function create()
    {
        if ($this->dbforge->create_database('AughtonVillageHall'))
        {
            echo 'Database created!';
        }
        $tracking = array(
                        'ID' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 5, 
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'TABLE' => array(
                                                 'type' => 'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'KEY' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'USERID' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'CHANGETYPE' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'DATETIME' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'NOTES' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),
                        'VALUE' => array(
                                                 'type' =>'INT',
                                                 'constraint' => '5',
                                                 'unsigned' => TRUE
                                          ),

                );
                $this->dbforge->add_field($tracking);
                $this->dbforge->add_key('ID');
                $this->dbforge->create_table('MYPOS_TRACKING');
    }
    function delete()
    {
        if ($this->dbforge->drop_database('AughtonVillageHall'))
        {
            echo 'Database dropped!';
        }
    }

}
?>