<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Breaks extends CI_Model {
    /* CodeIgniter Model
    Database: Breaks

    */

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function create($criteria=Null)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, roles.Name as role, PEOPLE.Visible as visible');
        $this->db->from('PEOPLE');
        $this->db->join('ROLES','ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    
    function read($criteria=Null)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, roles.Name as role, PEOPLE.Visible as visible');
        $this->db->from('PEOPLE');
        $this->db->join('ROLES','ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    function update($criteria=Null)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, roles.Name as role, PEOPLE.Visible as visible');
        $this->db->from('PEOPLE');
        $this->db->join('ROLES','ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    function exists($person_id)
    {
        $this->db->from('EMPLOYEES');   
        $this->db->join('PEOPLE', 'PEOPLE.person_id = EMPLOYEES.person_id');
        $this->db->where('EMPLOYEES.person_id',$person_id);
        $query = $this->db->get();
        
        return ($query->num_rows()==1);
    }   
    function detail($name)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, roles.Name as role, PEOPLE.Visible as visible');
        $this->db->from('PEOPLE');
        $this->db->where('PEOPLE.Name = "'.$name.'"');
        $this->db->join('ROLES','ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
}
?>