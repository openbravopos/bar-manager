<?php
/**
 * Cash Model
 *
 * 
 *
 * @package		MyPOS
 * @subpackage	Models
 * @category	Models
 * @author		Martyn Bristow
 * @link		http://martynbristow.co.uk/projects/bar-manager
 */
class Cash extends CI_Model {

	/**
	 * Cash queries
	 *
	 * 
	 *
	 * @param	string	$name	Marker name
	 * @return	void
	 */
    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->paymentmethods = array('cash', 'cashrefund', 'cheque', 'free', 'paperin', 'card', 'bank');
        $this->cashtransfer = array('cashin', 'cashout');
        $this->debt = array('debt', 'debtpaid');
    }
    /**
	 * Legacy Function: Now use: sales which can apply group bys
	 * @param bool $paymenttype A Boolean indicating if we should use a group by, default true
	 * @param 
	 */
    public function money()
    {
		$this->db->select('CLOSEDCASH.money as money, sum(PAYMENTS.`total`) as total, CLOSEDCASH.`DATESTART` as start, CLOSEDCASH.`DATEEND` as end, CLOSEDCASH.host as host, CLOSEDCASH.nosales as nosales, CLOSEDCASH.HOSTSEQUENCE	as sequence, PAYMENTS.PAYMENT as payment');
		$this->db->from('CLOSEDCASH');
		$this->db->join('RECEIPTS', 'CLOSEDCASH.money = RECEIPTS.money','left');
		$this->db->join('PAYMENTS', 'PAYMENTS.receipt = RECEIPTS.id ','left');
		$this->db->group_by('CLOSEDCASH.money');
		$this->db->group_by('PAYMENTS.PAYMENT');
		$this->db->order_by('start');
		return $this->db->get()->result();
    }

    /** Closed Cash
     * Get the closed cash between two dates: `start` and `end`
     * @param $start Start
     * @param $end End
     * @param $total Summarise payment types
     * @param $paymenttype Group by payment type
     */
    public function closed($start=NULL, $end=NULL, $total=FALSE, $paymenttype = TRUE) {
    	$this->_cash($total);
    	if(is_numeric($start) && ($start > 0))
			$this->db->where('RECEIPTS.`DATENEW` >=', date("Y-m-d H:i:s", $start));
    	else
    		log_message('DEBUG','Model Cash: Start Date! is less than 0, boundry error, ignoring');
    	if(is_numeric($end) && ($end < time() ))
    		$this->db->where('RECEIPTS.`DATENEW <=', date("Y-m-d H:i:s", $end));
    	else
    		log_message('DEBUG','Model Cash: End Date! is greater than now, boundry error, ignoring');
    	if($paymenttype) {
			$this->db->select('PAYMENTS.PAYMENT as payment');
			$this->db->group_by('PAYMENTS.PAYMENT');
		}
		return $this->db->get()->result_array();
    }
    /* Open Tills
    Select ONLY where the closed cash is NULL - i.e. OPEN
    */
    public function open($total=FALSE) {
    	$this->_cash($total);
    	$this->db->where('CLOSEDCASH.`DATEEND` is NULL');
		return $this->db->get()->result_array();
    }
    /* Master cash query
    */
    public function _cash($total)
    {
		$this->db->select_sum('PAYMENTS.`total`', 'total');
		$this->db->from('CLOSEDCASH');
		$this->db->where_in('PAYMENTS.PAYMENT', $this->paymentmethods);
		$this->db->join('RECEIPTS', 'CLOSEDCASH.money = RECEIPTS.money','left');
		if(!$total) {
			$this->db->group_by('CLOSEDCASH.money');
			$this->db->select('CLOSEDCASH.money as money, CLOSEDCASH.`DATESTART` as start, CLOSEDCASH.`DATEEND` as end, CLOSEDCASH.host as host, CLOSEDCASH.nosales as nosales, CLOSEDCASH.HOSTSEQUENCE	as sequence');
			$this->db->order_by('start');
		}
		$this->db->join('PAYMENTS', 'PAYMENTS.receipt = RECEIPTS.id ','left');
    }
    /**
     * Cash Transfers
     *
     * @param    string    $name    Marker name
     * @return    void
     */
    public function cashtransfer()
    {
		$this->db->select('CLOSEDCASH.money as money, sum(PAYMENTS.`total`) as total, CLOSEDCASH.`DATESTART` as start, CLOSEDCASH.`DATEEND` as end, CLOSEDCASH.host as host, CLOSEDCASH.nosales as nosales, CLOSEDCASH.HOSTSEQUENCE	as sequence');
    }
    /**
     * Summarised closed cash
     *
     * @param    string    $key    Marker name
     * @return   database Database result
     */
    public function summary($key)
    {
		$this->db->select('sum(payments.`total`) as total, CLOSEDCASH.`DATESTART` as start, CLOSEDCASH.`DATEEND` as end, CLOSEDCASH.host as host, , CLOSEDCASH.HOSTSEQUENCE	as sequence');
		$this->db->from('CLOSEDCASH');
		$this->db->join('RECEIPTS', 'CLOSEDCASH.money = RECEIPTS.money','left');
		$this->db->join('payments', 'payments.receipt = RECEIPTS.id ','left');
		$this->db->where('CLOSEDCASH.money = "'.$key.'"');
		return $this->db->get()->result();
	}
    /**
     * Detailed closed cash
     *
     * @param    string    $key    Closed cash ID
     * @return   database Database result
     */
    public function detail($key)
    {
	    $this->db->select('CLOSEDCASH.money as `key`, CLOSEDCASH.host as host, RECEIPTS.datenew as date, TICKETLINES.price as price, TICKETLINES.units as units, PRODUCTS.name as product');
		$this->db->join('TICKETS', 'TICKETS.id = TICKETLINES.ticket');
		$this->db->join('RECEIPTS', 'TICKETS.id = RECEIPTS.id ');
		$this->db->join('CLOSEDCASH', 'CLOSEDCASH.money = RECEIPTS.money ');
		$this->db->join('PRODUCTS', 'PRODUCTS.id = TICKETLINES.product ');
		$this->db->from('TICKETLINES');
		$this->db->where('CLOSEDCASH.money',$key);
		$this->db->order_by('date');
		return $this->db->get()->result();
    }
}
?>