<?php
/*
Manage the users of the application
*/
class Resources extends CI_Model {

    var $ID   = 0;
    var $NAME = "";
    var $RESTYPE = 0;
    var $CONTENT = "";

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function index() {
        $this->db->select('ID, NAME, RESTYPE as TYPE');
        return $this->db->get('RESOURCES')->result();
    }
    function read($key, $value)
    {
        $query = $this->db->get_where('RESOURCES', array($key, $value));
        return $query->result();
    }
    function create()
    {
        $this->db->insert('RESOURCES', $this);
    }
    function update($key, $value)
    {
        $this->db->update('RESOURCES', $this);
    }
    function delete($key, $value)
    {
        $this->db->update('RESOURCES', $this, array($key => $value));
    }
    function content($id) {
        $this->db->select('RESTYPE, CONTENT');
        $row = $this->db->get_where('RESOURCES', array('id' => $id))->row();
        //if($row->RESTYPE) return $row->CONTENT;
        //else return Null;
        return $row->CONTENT;
    }
}
?>