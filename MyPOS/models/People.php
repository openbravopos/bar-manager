<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* CodeIgniter Model
 * Application: MyPOS
 * Author: Martyn Bristow
 * Copyright: Martyn Bristow 2015
 * License: GPL
*/
class People extends CI_Model {
    /* CodeIgniter Model
    Database: People

    */
    var $ID = 0;
    var $NAME = '';
    var $APPPASSWORD    = '';
    var $CARD = NULL;
    var $ROLE = 0;
    var $VISIBLE = 0;
    var $IMAGE = NULL;

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    // Fetch the User list
    function get($key=Null, $value=Null)
    {
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, ROLES.Name as role, PEOPLE.Visible as visible');
        if(!is_null($key))
            if($this->db->field_exist($key, 'PEOPLE'))
                $this->db->where($key, $value);
            else
                $this->messages->add("Criteria ".$key." not valid for table PEOPLE", 'error');
        $this->db->from('PEOPLE');
        $this->db->join('ROLES', 'ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    // Does the given person_id exist
    function exists($person_id)
    {
        $this->db->from('EMPLOYEES');   
        $this->db->join('PEOPLE', 'PEOPLE.person_id = EMPLOYEES.person_id');
        $this->db->where('EMPLOYEES.person_id',$person_id);
        $query = $this->db->get();
        
        return ($query->num_rows()==1);
    }
    // Get details about a given user by ID
    function detail($id)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('PEOPLE.ID as id, PEOPLE.Name as name, ROLES.NAME as role, PEOPLE.Visible as visible');
        $this->db->from('PEOPLE');
        $this->db->where('PEOPLE.ID = "'.$id.'"');
        $this->db->join('ROLES','ROLES.id = PEOPLE.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    // Create a new user
    function create() {
        $this->ID = $this->_next(); // Get the next UID, this is not an autoincrement
        $this->db->insert('PEOPLE', $this);
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('PEOPLE', $data);
    }
    // Delete the given user
    function delete($id) {
        $this->db->delete('mytable', array('id' => $id)); 
    }
    // Get the next available UserID
    function _next() {
        $this->db->select('ID');
        return max(array_filter(array_column($this->db->get('PEOPLE')->result_array(),'ID'),'is_numeric'))+1;
    }
    // Fetch a users image
    function img($id) {
        $this->db->select('IMAGE');
        return array_column($this->db->get_where('PEOPLE', array('id' => $id))->result_array(), 'IMAGE')[0];
    }

}
?>