<?php
/*
Manage the users of the application
*/
class Users extends CI_Model {

    var $id   = 0;
    var $username = "";
    var $password = "";
    var $name = ""; // Users fullname
    var $people = 0;// Relate this person to a POS person
    var $permission = 0;
    var $modified_date = 0;
    var $modified_user = 0;

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function list()
    {
        $this->db->select('username, permission, name');
        $this->db->get('mytable');
    }
    function read($username)
    {
        $this->db->select('id, username, name, people, permission, modified_date, modified_user');
        $query = $this->db->get_where('users', array('username' => $username));
    }
    function create()
    {
        $this->username = "";
        $this->password = "";
        $this->name = $this->input->
        $this->people = 0;
        $this->permission = 0;
        $this->modified_date = time();
        $this->modified_user = $this->currentuser();
        $this->notes = "";

        $this->db->insert('people', $this);
    }

    function update($username)
    {
        $this->db->update('users', $this, array('username',$username));
    }
    function setPassword($username, $password)
    {
        $this->db->update('users', array('password'=>password_hash($password, PASSWORD_BCRYPT), array('username',$username));
    }

}
?>