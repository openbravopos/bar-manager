<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ticketlines extends CI_Model {
    /* CodeIgniter Model
    Database: People

    */

    var $NAME = '';

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    public function _ticketlines()
    {
        $this->db->select('TICKETLINES.TICKET as ticket, TICKETLINES.UNITS as no,TICKETLINES.PRICE as price, PRODUCTS.NAME as name, PRODUCTS.CODE as code');
        $this->db->from('TICKETLINES');
        $this->db->join('PRODUCTS','PRODUCTS.ID = TICKETLINES.Product','left');
        return $this->db->get()->result();
    }
    public function summary($start, $end) {

    }
    public function event() {

    }


}
?>