<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class People extends CI_Model {
    /* CodeIgniter Model
    Database: People

    */

    var $NAME = '';
    var $APPPASSWORD    = '';
    var $CARD = NULL;
    var $ROLE = 0;
    var $VISIBLE = 0;
    var $IMAGE = NULL;

    public function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function get($criteria=Null)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('people.ID as id, people.Name as name, roles.Name as role, people.Visible as visible');
        $this->db->from('people');
        $this->db->join('roles','roles.id = people.Role','left');
        $people = $this->db->get();
        return $people->result();
    }
    function exists($person_id)
    {
        $this->db->from('employees');   
        $this->db->join('people', 'people.person_id = employees.person_id');
        $this->db->where('employees.person_id',$person_id);
        $query = $this->db->get();
        
        return ($query->num_rows()==1);
    }   
    function detail($name)
    {
        /*if not(is_null($criteria)) {
            print 'no';
        }*/
        $this->db->select('people.ID as id, people.Name as name, roles.Name as role, people.Visible as visible');
        $this->db->from('people');
        $this->db->where('people.Name = "'.$name.'"');
        $this->db->join('roles','roles.id = people.Role','left');
        $people = $this->db->get();
        return $people->result();
    }

}
?>