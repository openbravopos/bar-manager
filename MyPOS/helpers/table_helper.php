<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('table'))
{
	/**
	 * Creates a state/provience/county dropdown form input based on the entries in the address.states config.
	 * The word "state" is used but the structure can be used for Canadian proviences, Irish and UK counties and
	 * any other area based data.
	 *
	 * @param $selected_code string The value of the item that should be selected when the dropdown is drawn.
	 * @param $default_abbr string The value of the item that should be selected if no other matches are found.
	 * @param $country_code string The code of the country for which the states/priviences/counties are returned. Defaults to 'US'.
	 * @param $select_name string The name assigned to the select. Defaults to 'state_code'.
	 * @param $class string Optional value for class name.
	 *
	 * @return string The full html for the select input.
	 */
	function table($data, $fields=Null, $callbacks=array())
	{
		$s = array("<table class='table tablesorter'><thead><tr>");
		if(is_null($fields)) $fields = array_keys(get_object_vars($data[0]));
		//$fields = array_keys($data[0]);
		foreach ($fields as $key => $value) {
			//if(is_int($key)
				$s[]='<th>'.$value."</th>";
			}
		$s[]="</tr></thead><tbody>";
		foreach ($data as $row) {
			$s[]="<tr>";
			foreach ($fields as $key) {
				$row = (array)$row;
				if(array_key_exists($key, $callbacks)) $s[]='<td>'.call_user_func($callbacks[$key],$row[$key])."</td>";
				else $s[]='<td>'.$row[$key]."</td>";
			}
			$s[]="</tr>";
		}
		$s[]="</tbody></table>";
		return implode("",$s);
	}
}
?>