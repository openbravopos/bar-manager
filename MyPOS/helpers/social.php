<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('social'))
{
	/**
	 * Creates a social block as part of a list.
	 *
	 * @param $data string The value of the item that should be selected when the dropdown is drawn.
	 *
	 * @return string The social link.
	 */
	function social($social)
	{
		$str = "<ul>";
		foreach($social as $link) {
			$str += "<li><a href='".$link["href"]."' class='icon ".$link["fa"].".><span class='label'>".$link["name"]."</span></a></li>";
		}
		$str += "<ul>";
		return $str;
	}
}
?>