<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('money'))
{
	/**
	 * Creates a state/provience/county dropdown form input based on the entries in the address.states config.
	 * The word "state" is used but the structure can be used for Canadian proviences, Irish and UK counties and
	 * any other area based data.
	 *
	 * @param $selected_code string The value of the item that should be selected when the dropdown is drawn.
	 * @param $default_abbr string The value of the item that should be selected if no other matches are found.
	 * @param $country_code string The code of the country for which the states/priviences/counties are returned. Defaults to 'US'.
	 * @param $select_name string The name assigned to the select. Defaults to 'state_code'.
	 * @param $class string Optional value for class name.
	 *
	 * @return string The full html for the select input.
	 */
	function money($amount)
	{
		return "&pound;".number_format($amount, 2);;

	}
}
if ( ! function_exists('datef'))
{
	/**
	 * Creates a state/provience/county dropdown form input based on the entries in the address.states config.
	 * The word "state" is used but the structure can be used for Canadian proviences, Irish and UK counties and
	 * any other area based data.
	 *
	 * @param $selected_code string The value of the item that should be selected when the dropdown is drawn.
	 * @param $default_abbr string The value of the item that should be selected if no other matches are found.
	 * @param $country_code string The code of the country for which the states/priviences/counties are returned. Defaults to 'US'.
	 * @param $select_name string The name assigned to the select. Defaults to 'state_code'.
	 * @param $class string Optional value for class name.
	 *
	 * @return string The full html for the select input.
	 */
	function datef($str)
	{
		return date("d M Y H:m",strtotime($str));

	}
}
?>