<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('exist'))
{
	/**
	 * Creates a social block as part of a list.
	 *
	 * @param $data string The value of the item that should be selected when the dropdown is drawn.
	 *
	 * @return string The social link.
	 */
	function exist($file)
	{
		$CI =& get_instance();
		$appname = $CI->config->item('appname', 'application');
		$views = $CI->config->item('views', 'application');
		$ext = $CI->config->item('ext', 'application');
		log_message(join(DIRECTORY_SEPARATOR, array($appname, $views, $file).$ext))
		return file_exists(join(DIRECTORY_SEPARATOR, array($appname, $views, $file).$ext));
	}
}
?>