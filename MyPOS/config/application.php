<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['application.name'] = 'Bar Manager';
$config['application.org'] = 'Aughton Village Hall';
$config['application.appname'] = 'Bar Manager';
$config['application.orgname'] = 'Aughton Village Hall';
$config['application.theme'] = 'simple';//'default';
$config['application.theme.front'] = 'default';
$config['application.theme.app'] = 'default';
$config['application.default.front'] = 'welcome';
$config['application.views'] = 'views';
$config['application.base'] = 'application';
$config['application.ext'] = '.php';

$config['theme.banner'] = true;
$config['theme.avatar'] = true;

$config['site.default_user_timezone'] = 'UM8';

//--------------------------------------------------------------------
// !BACKUPS
//--------------------------------------------------------------------
$config['site.backup_folder']	= 'archives/';
$config['system.commandlocation'] = "/tmp/phpsys.";

$config['social'] = array(
	'Facebook'=>'http://facebook.com/aughtonvillagehall',
	'Twitter' => 'http://twitter.com/aughtonvillageh',
	'Email' => 'mailto:admin@aughtonvillagehall.co.uk'
	);
$config['logos'] = array(
	'Facebook'=>'fa-facebook',
	'Twitter' => 'fa-twitter',
	'Email' => 'fa-envelope'
	);

$config['static'] = array(
	'path' => 'assets/',
	'js' => array('js/core.js'),
	'css' => array('themes/default/css/securearea.css'),
	'packages' => array('jquery', 'jquery-ui', 'fancybox', 'tablesorter', 'bootstrap','d3','fontawesome','angular')
	);

$config['packages'] = array(
	'jquery'=> array('js/jquery-2.1.1.min.js'),
	'fancybox' => array('FancyBox/jquery.fancybox.css','FancyBox/helpers/jquery.fancybox-thumbs.css','FancyBox/helpers/jquery.fancybox-buttons.css','FancyBox/jquery.fancybox.pack.js',	
		'FancyBox/helpers/jquery.fancybox-buttons.js','FancyBox/helpers/jquery.fancybox-media.js','FancyBox/helpers/jquery.fancybox-thumbs.js'),
	'jquery-ui' => array('JQueryUI/jquery-ui.min.css','JQueryUI/jquery-ui.structure.min.css','JQueryUI/jquery-ui.theme.min.css','JQueryUI/jquery-ui.min.js'),
	'tablesorter' => array('tablesorter/jquery.tablesorter.min.js', 'tablesorter/themes/green/style.css'),
	'bootstrap' => array('bootstrap/dist/css/bootstrap.min.css', 'bootstrap/dist/js/bootstrap.min.js'),
	'd3' => array('d3/d3.min.js'),
	'fontawesome' => array('font-awesome/css/font-awesome.min.css'),
'angular'=> array('angular/angular.min.js')
	);


