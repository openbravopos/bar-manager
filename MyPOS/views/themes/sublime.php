<!DOCTYPE html>
<?php $themedir = base_url()."assets/themes/"."sublime/";?>
<html lang="en">
	<head>
	<base href="<?=base_url();?>" />
		<title><?=$title; ?></title>
	<meta charset="utf-8">
	<meta name="author" content="pixelhint.com">
	<meta name="description" content="Sublime Stunning free HTML5/CSS3 website template"/>
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/reset.css">
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/fancybox-thumbs.css">
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/fancybox-buttons.css">
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/fancybox.css">
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?=$themedir?>css/main.css">

    <script type="text/javascript" src="<?=$themedir?>js/jquery.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/fancybox.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/fancybox-buttons.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/fancybox-media.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/fancybox-thumbs.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/wow.js"></script>
    <script type="text/javascript" src="<?=$themedir?>js/main.js"></script>

    			<?php
	/** -- Copy from here -- */
	if(!empty($meta))
	foreach($meta as $name=>$content){
		echo "\n\t\t";
		?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
			 }
	echo "\n";

	if(!empty($canonical))
	{
		echo "\n\t\t";
		?><link rel="canonical" href="<?php echo $canonical?>" /><?php

	}
	echo "\n\t";
	foreach($css as $file){
	 	echo "\n\t\t";
		?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
	} echo "\n\t";

	foreach($js as $file){
			echo "\n\t\t";
			?><script src="<?php echo $file; ?>"></script><?php
	} echo "\n\t";

	/** -- to here -- */
?>
</head>
<body>

	<section class="billboard light">
		<header class="wrapper light">
			<a href="#"><img class="logo" src="<?=$themedir?>img/logo_light.png" alt=""/></a>
			<nav>
				<ul>
					<li><a href="">About Us</a></li>
					<li><a href="">Products</a></li>
					<li><a href="">Journal</a></li>
					<li><a href="">Contact Us</a></li>
				</ul>
			</nav>
		</header>

		<div class="caption light animated wow fadeInDown clearfix">
			<h1>Sunt In Culpa Officia Deserunt</h1>
			<p>dolore magna aliqua enim ad minim veniam occaecat</p>
			<hr>
		</div>
		<div class="shadow"></div>
	</section><!--  End billboard  -->
<?=$output?>


	<footer>
		<div id="copy">
			<!--<img src="<?=$themedir?>img/footer_logo.png" alt="" class="footer_logo"/>-->
			<p>&copy; Martyn Bristow 2015</p>
		</div>

			<nav>
				<ul>
					<li><a href="#">About</a></li>
					<li><a href="#">FAQ</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</nav>
		</div>		
	</footer><!--  End footer  -->
</body>
</html>