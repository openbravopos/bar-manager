<!DOCTYPE html>
<!-- HTML5 theme for FrontEnd pages-->
<html lang="en">
	<head>
		<title><?php echo $title; ?></title>
		<meta name="resource-type" content="document" />
		<meta name="robots" content="all, index, follow"/>
		<meta name="googlebot" content="all, index, follow" />
	<?php
	/** -- Copy from here -- */
	if(!empty($meta))
	foreach($meta as $name=>$content){
		echo "\n\t\t";
		?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
			 }
	echo "\n";

	if(!empty($canonical))
	{
		echo "\n\t\t";
		?><link rel="canonical" href="<?php echo $canonical?>" /><?php

	}
	echo "\n\t";
	foreach($css as $file){
	 	echo "\n\t\t";
		?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
	} echo "\n\t";

	foreach($js as $file){
			echo "\n\t\t";
			?><script src="<?php echo $file; ?>"></script><?php
	} echo "\n\t";

	/** -- to here -- */
?>

    <!-- Le styles
    <link href="<?php echo base_url(); ?>assets/themes/default/hero_files/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/themes/default/hero_files/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/themes/default/css/general.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/themes/default/css/custom.css" rel="stylesheet"> -->
    <link href="<?php echo base_url(); ?>assets/themes/default/css/front.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/default/favicon.ico" type="image/x-icon"/>
	<meta property="og:image" content="<?php echo base_url(); ?>assets/themes/default/images/facebook-thumb.png"/>
	<link rel="image_src" href="<?php echo base_url(); ?>assets/themes/default/images/facebook-thumb.png" />
</head>
<body>
<header style='background-image:url(<?=base_url();?>/assets/themes/default/images/banner.jpg); height:250px;"'>
	<?=$title; ?>
	<nav>
		<ul>
			<li>Log In</li>
		</ul>
	</nav>
	<div class="clear">&nbsp;</div>
</header>
<!--<section>
	<img class="banner" src="<?=base_url();?>/assets/themes/default/images/banner.jpg"/>
</section>-->
<section>
	<?=$output;?>
</section>
<section></section>
<footer></footer>
<!--
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <img src="<?php echo base_url(); ?>assets/themes/default/images/logo.png" style="float:left;margin-top:5px;z-index:5" alt="logo"/>
          <a class="brand" href="<?php echo site_url(); ?>">&nbsp;&nbsp;<?=$this->config->item("application.brandname");?></a>
          <div style="height: 0px;" class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
    
	    <?php if($this->load->get_section('text_header') != '') { ?>
	    	<h1><?php echo $this->load->get_section('text_header');?></h1>
	    <?php }?>
	    <div class="row">
		    <?php echo $output;?>
			<?php echo $this->load->get_section('sidebar'); ?>
	    </div>
	    <hr/>

	    <footer>
	      	<div class="row"></div>
	    </footer>
    </div>
    -->
</body>
</html>
