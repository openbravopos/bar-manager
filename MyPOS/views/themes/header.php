<!-- Header -->
<div id="header" class="skel-layers-fixed">
	<div class="top">
		<!-- Logo -->
		<div id="logo">
			<?php //if $this->config->item('theme.banner') ?>
			<span class="image banner"><img src="<?=base_url()?>assets/images/banner.jpg" alt="" /></span> <!--class="image avatar48"-->
			<h1 id="title"><?=isset($name)?$name:$this->config->item('application.orgname');?></h1>
			<p><?=isset($desc)?$desc:$this->config->item('application.appname');?></p>
		</div>
		<?=$this->load->get_section('nav');?>
	</div>
	<?=$this->load->get_section('social');?>
</div>