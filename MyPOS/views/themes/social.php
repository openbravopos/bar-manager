<div class="bottom">
<!-- Social Icons -->
<ul class="icons">
<?php 
$logos = $this->config->item('logos');
foreach($this->config->item('social') as $service => $url):?>
	<li><a href="<?=$url?>" class="icon <?=$logos[$service]?>"><span class="label"><?=$service?></span></a></li>
<?php endforeach;?>
</ul>
</div>