<!-- Nav -->
<nav id="nav">
<ul>
	<li><a href="<?=site_url()?>" id="home"><span class="icon fa-home">Home</span></a></li>
	<?php if(isset($nav)):
	foreach($nav as $link):?>
		<li><a href="#portfolio" id="portfolio-link" class="skel-layers-ignoreHref"><span class="icon fa-th">Portfolio</span></a></li>
	<?php endforeach; endif;?>
	<li><a href="/login" id="login"><span class="icon fa-unlock">Login</span></a></li>
	<li><a href="/logout" id="logout"><span class="icon fa-lock">Logout</span></a></li>
</ul>
</nav>