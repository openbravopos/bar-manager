<!DOCTYPE HTML>
<?php $themedir = base_url()."assets/themes/"."prologue/";?>
<html>
	<head>
	<!--<base href="<?=base_url();?>" />-->
		<title><?=$title; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
			<?php
	/** -- Copy from here -- */
	if(!empty($meta))
	foreach($meta as $name=>$content){
		echo "\n\t\t";
		?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
			 }
	echo "\n";

	if(!empty($canonical))
	{
		echo "\n\t\t";
		?><link rel="canonical" href="<?php echo $canonical?>" /><?php

	}
	echo "\n\t";
	foreach($css as $file){
	 	echo "\n\t\t";
		?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
	} echo "\n\t";

	foreach($js as $file){
			echo "\n\t\t";
			?><script src="<?php echo $file; ?>"></script><?php
	} echo "\n\t";

	/** -- to here -- */
?>
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<!--<script src="<?=$themedir?>js/jquery.min.js"></script>-->
		<script src="<?=$themedir?>js/jquery.scrolly.min.js"></script>
		<script src="<?=$themedir?>js/jquery.scrollzer.min.js"></script>
		<script src="<?=$themedir?>js/skel.min.js"></script>
		<script src="<?=$themedir?>js/skel-layers.min.js"></script>
		<script src="<?=$themedir?>js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="<?=$themedir?>css/skel.css" />
			<link rel="stylesheet" href="<?=$themedir?>css/style.css" />
			<link rel="stylesheet" href="<?=$themedir?>css/style-wide.css" />
		</noscript>
		<link rel="stylesheet" href="<?=base_url()?>assets/css/default.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<?=$this->load->get_section('header');?>

		<!-- Main -->

		<div id="main">
		<?=$this->load->get_section('banner');?>
		<?=$this->load->get_section('messages');?>
		<?=$output?>
		</div>
		<?=$this->load->get_section('footer');?>


	</body>
</html>