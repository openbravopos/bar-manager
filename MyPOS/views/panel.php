<div class="panel panel-primary">
	<div class="panel-heading"><?=$title?></div>
  <div class="panel-body">
    <?=$data?>
  </div>
  <div class="panel-footer"><?=$footer?></div>
</div>