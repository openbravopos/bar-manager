<?php if(isset($title)):?>
<h1><?=$title?></h1>
<?php endif; ?>
<table class='tablesorter'>
<?php if(isset($caption)):?>
<caption><?=$caption?></caption>
<?php endif; ?>
<thead>
<tr>
<?php if(isset($header)) {
	foreach($header as $cell) { ?>
		<th><?=$cell;?></th>
	<?php }
} ?>
</tr>
</thead>
<tbody>
<?php foreach($table as $row) { ?>
	<tr>
	<?php foreach($header as $head) {?>
		<td><?=$row[$head];?></td>
	<?php }?>
	</tr>
<?php }?>
</tbody>
<tfoot></tfoot>
</table>
<?php if(isset($pageination)) echo $pageination;
?>
<?php if(isset($footer)) echo $footer;
?>
