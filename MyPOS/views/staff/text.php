<?php $base='staff/detail/'?>
<h1>TABLE</h1>
<table>
<tr><th>Name</th><th>Role</th></tr>
<?php foreach($stafflist as $staff): ?>
	<?php if($staff->visible):?>
	<tr class='active'><td><a href="<?=$base.$staff->name;?>"><?=$staff->name;?></a></td><td><?=$staff->role;?></td><td>Deactivate</td></tr>
	<?php else: ?>
	<tr class='inactive'><td><a href="<?=$base.$staff->name;?>"><?=$staff->name;?></a></td><td><?=$staff->role;?></td><td>Activate</td></tr>
	<?php endif;?>
<?php endforeach; ?>
</table>