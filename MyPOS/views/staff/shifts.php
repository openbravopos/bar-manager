<?php $base='staff/detail/'?>
<h1>Shifts</h1>
<table>
<tr><th>Name</th><th>Date</th><th>Start Time</th><th>End Time</th><th>Hours</th></tr>
<?php foreach($shifts as $shift): 

	$start = new DateTime($shift->start);
	$end = new DateTime($shift->end);
	$interval = date_diff($end, $start);
	?>
	<tr> <td><a href="<?=$base.$shift->name;?>"><?=$shift->name;?></a></td> <td><?=$start->format('d-m-y');?></td> <td><?=$start->format('H:i');?></td> <td><?=$end->format('H:i');?></td> <td><?=$interval->format('%H:%i');?></td><td><i class="fa fa-pencil"></i></td></tr>

<?php endforeach; ?>
</table>