<?php
echo form_open('users/newuser','class="form"');

$data = array(
              'name'        => 'username',
              'id'          => 'username',
              'maxlength'   => '30',
              'size'        => '30',
            );
echo form_label('Username', 'username');
echo form_input($data);

$data = array(
              'name'        => 'password',
              'id'          => 'password',
              'maxlength'   => '20',
              'size'        => '20',
            );
echo form_label('Password:', 'password');
echo form_password($data);

echo form_submit('login', 'Submit','id="submit"');

echo "<div class='clear'>&nbsp;</div>";echo form_close();