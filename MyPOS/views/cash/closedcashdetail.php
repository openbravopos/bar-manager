<p>&gt; <a href="<?=site_url()?>">Dashboard</a> &gt; <a href="<?=site_url().'/'.$this->uri->rsegment(1)?>">Takings</a></p>
<p>Date: <?=$date?></p>
<p>Total Taking: <?=$total?></p>
<table class='tablesorter'>
<thead>
	<tr>
	<th>Product</th>
	<th>Price</th>
	<th>Units</th>
	<th>Total</th>
	</tr>
	</thead>
	<tbody>
<?php foreach($sales as $row):?>
	<tr>
	<td><?=$row->product?></td>
	<td><?=money($row->price);?></td>
	<td><?=$row->units;?></td>
	<td><?=money($row->units*$row->price);?></td>
	</tr>
<?php endforeach; ?>
</tbody>
</table>