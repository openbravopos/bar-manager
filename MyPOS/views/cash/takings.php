<h1>Takings Summary</h1>

<?php $base='closedcash/detail/'?>
<table class='tablesorter'><thead>
<tr>
<th>Start Date</th><th>End Date</th><th>Total</th><th>Payment Type</th><th>No Sales</th><th>Host</th><th>...</th>
</tr></thead><tbody>
<?php foreach($sales as $row): ?>
	<tr>
	<td><?=date("d M Y H:m",strtotime($row->start));?></td>
	<td><?=date("d M Y H:m",strtotime($row->end));?></td>
	<td>&pound;<?=number_format($row->total, 2);?></td>
	<td><?=$row->payment;?></td>
	<td><?=$row->nosales;?></td>
	<td><?=$row->host;?></td>
	<td><a href="<?=$base.$row->money;?>">Details</a></td>
	</tr>
<?php endforeach; ?></tbody>
</table>
<?php foreach($total as $row): ?>
<?=$row->payment;?> &pound;<?=number_format($row->total, 2);?>

<?php endforeach; ?>