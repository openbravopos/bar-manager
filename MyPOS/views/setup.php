<!-- Application Installer - Navbar and messages -->
<h1><?=$this->config->item('application.name');?> - Setup</h1>
<nav>
<ul>
	<li><a href="<?=site_url('setup/info/unicenta');?>">Install POS</a></li>
	<li><a href="<?=site_url('setup/install');?>">Install App</a></li>
	<li><a href="<?=site_url('setup/uninstall');?>">Uninstall App</a></li>
	<li><a href="<?=site_url('setup/info/unicenta');?>">Uninstall POS</a></li>
</ul>
<p>&nbsp;</p>
</nav>
<?php
if(isset($title))
	echo "<h2>".$title."</h2>";
if(isset($message))
	echo "<p class='message'>".$message."</p>";
if(isset($messages)) {
	foreach($messages as $key => $value) {
		//'<p>'.print_r($value).'</p>';
		echo "<p class='message'>".(is_string($key) ? $key."=":"").$value."</p>";
	}
}