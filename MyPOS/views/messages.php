<div id="messages">
<?php foreach($this->messages->get("error") as $error):?>
	<div class="alert alert-danger" role="alert"><?=$error;?></div>
<?php endforeach;?>
<?php foreach($this->messages->get("message") as $message):?>
	<div class="alert alert-info" role="alert"><?=$message;?></div>
<?php endforeach;?>
<?php foreach($this->messages->get("success") as $success):?>
	<div class="alert alert-success" role="alert"><?=$success;?></div>
<?php endforeach;?>
</div>
<?php if(isset($_SESSION['message'])):?>
<div class="alert alert-info" role="alert"><?=$_SESSION['message']?></div>
<?php endif;?>
