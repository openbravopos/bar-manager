<form action="<?=isset($action) ? $action : current_url()?>" method="post" accept-charset="UTF-8" enctype="application/x-www-form-urlencoded">
  <fieldset>
    <legend><?=$title?></legend>
    <?php foreach($elements as $element):?>
    	<label><?=$element['label']?></label>
    	<input type="<?=$element['type']?>" name="<?=$element['name']?>" value="<?=isset($element['value'])?$element['value']:''?>">
	<?php endforeach;?>
    <input type="submit" value="<?=$submit?>">
  </fieldset>
</form>