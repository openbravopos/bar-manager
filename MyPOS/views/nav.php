<?php if(isset($nav)): ?>
<nav>
	<ul>
	<?php foreach($nav as $href => $title): ?>
		<li><a href="<?=site_url();?>/<?=$href;?>"><?=$title;?></a></li>
	<?php endforeach; ?>
	</ul>
</nav>
<?php endif; ?>