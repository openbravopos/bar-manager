<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once('Authenticated_Controller.php');
class Dashboard extends Authenticated_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->output->set_common_meta('Dashboard', '', '');

	}
	public function index()
	{
		$this->load->model('Shiftsmodel');
		$this->load->model('cash');
		$this->load->helper('table');
		$data['tiles'] = array(
			$this->load->view('panel',
				array(
				'title'=> 'Logged In',
				'footer'=> '',
				'data' => table($this->Shiftsmodel->logged_in(), array('name','start'))
				), true),
			$this->load->view('panel',
				array(
				'title'=> 'Takings',
				'footer' => '',
				'data' => table($this->cash->open(),array('host','total'))
				), true)
		);
		$this->load->view('dashboard/index', $data);
	}
}