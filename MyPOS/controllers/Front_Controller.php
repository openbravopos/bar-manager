<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the CI_Controller for the website/app frontend

No restrictions or security
*/

class Front_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->security->get_logged_in_employee_info();
		$this->load->library('application');
		$this->load->helper('url');
		$this->load->js('assets/js/jquery-2.1.1.min.js');
		$this->load->css('assets/FancyBox/jquery.fancybox.css');
		$this->load->css('assets/FancyBox/helpers/jquery.fancybox-thumbs.css');
		$this->load->css('assets/FancyBox/helpers/jquery.fancybox-buttons.css');
		$this->load->js('assets/FancyBox/jquery.fancybox.pack.js');	
		$this->load->js('assets/FancyBox/helpers/jquery.fancybox-buttons.js');
		$this->load->js('assets/FancyBox/helpers/jquery.fancybox-media.js');
		$this->load->js('assets/FancyBox/helpers/jquery.fancybox-thumbs.js');
		$this->load->js('assets/js/core.js');
		$this->load->css('assets/themes/default/css/bootstrap.min.css');
		$this->load->js('assets/themes/default/js/bootstrap.min.js');
		$this->_init();
	}

	public function _init()
	{
		//$this->output->set_template('sublime');
		$this->output->set_template('prologue');
		$this->load->section('nav','themes/nav');
		$social['data'] = array();
		$this->load->section('social','themes/social', $social);
		$this->load->section('header','themes/header');
		$this->load->section('footer', 'themes/footer');
		$this->load->section('banner', 'themes/banner');
		$this->load->section('user', 'login');
		
	}

	public function index()
	{

	}
}