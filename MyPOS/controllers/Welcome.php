<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front End controller for WebApp
Front Splash Screen with login

Requires:
Application to be installed, if the applciation is not installed you will be redirected onto the installer or splash screen
*/


require_once('Front_Controller.php');
class Welcome extends Front_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->application->installed()[0])	// Not Installed - application::installed returns an array (errorcount, error nos)
		{
			log_message('debug','Application not installed');
			$target = 'setup'; //$this->config->item('application.default.front');
			if(file_exists('application/controllers/'.$target.'.php')){
				log_message('debug','Redirecting to '.$target);
				redirect($target, array('message' => 'Application not installed'));
			}
			else
				show_error('Unable to redirect, target controller doesn`t exist');			
		}
		$this->load->library('auth');
		//$this->security->get_logged_in_employee_info();
		$this->_init();
		$this->output->set_common_meta($this->config->item('application.name'), '', '');
	}

	public function index()
	{	
		if($this->auth->is_logged_in())
			redirect('dashboard');
		else
		{
			$this->load->helper('form');
			//$d['widgets']['login'] = $this->load->view('login', False, True);
			$this->load->view('welcome');
		}

	}
	/* Application Login
	*/
	public function login()
	{	
		// Check for a current session
		if($this->auth->is_logged_in())
			redirect('welcome');
		$this->load->helper('form');
		$this->output->set_common_meta('Login', '', '');
		// Form Submitted
		log_message('debug','Login Method'.$this->input->post('username'));
		if($this->input->post('username'))
		{
			$user = $this->input->post('username');
			$pass = $this->input->post('password');
			$result = $this->auth->login($user,$pass);
			if($this->input->is_ajax_request())
				//print "AJAX";
				exit;
			else {
				if($result)	{
					log_message('debug','Redirecting to dashboard');
					//redirect('dashboard');
					header('Location: '.site_url().'/dashboard');
				}
				else {
					log_message('debug','Login error');
					$this->load->view('login');

					$this->output->set_status_header(401)
			        	->set_content_type('text/html', 'UTF-8');
			        	//->set_output($this->output->get_output())
			        	//->_display();
					//exit;
				}
			}
		}
		// Show login form
		else
			$this->load->view('login');
			/*$this->output
				->set_status_header(200)
			    ->set_content_type('text/html', 'UTF-8')
			    ->set_output($this->load->view('login',array(),True))
			    ->_display();
			exit;
			*/
	}
	public function logout()
	{
		if($this->application->installed())
		{
			$this->load->library('auth');
			if($this->auth->logout())
				//redirect('welcome');
				$this->load->view('login');
		}
		else
		{
			show_error('Unable to run application, not installed!');
		}
	}
	public function test()
	{
		print_r($this->session->all_userdata());
	}
	public function server($opt = 'poweroff')
	{
		$this->load->helper('file');
		$data = '';
		$loc = '/tmp/php_system.';
		$loc = $this->config->item('system.commandlocation');
		$fname = $loc.'poweroff';
		if ( ! write_file($fname, $data))
		{
			log_message('error','Cannot write shutdown file: '.$fname);
		     $this->messages->add("Unable to write the file", "errror");
		}
		else
		{
		     $this->messages->add('Server shutdown requested.', "success");
		     log_message('info','Shutdown requested. Written: '.$fname);
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */