<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
# We require the basic unauthenticated controller
require_once('Front_Controller.php');

/* MyPOS application setup
 * Provide installation, update, uninstall
 * Permission: None
 * 
*/

// Tables related to this application - These are dropped or 
$tables = array('session', 'users', 'messages', 'security_log');
$redirect = 'dashboard';

class Setup extends Front_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
        $this->load->library('application');
    }

    /* Launch the application setup
    */
    public function index()
    {
        $this->load->view('setup');
    }

    public function info($page=False)
    {
    }

    /* Install:
    Create tables: users
    Setup sessions for CI
    Setup the default user
    */
    public function install()
    {
        $opt = func_get_args();
        log_message('debug', 'Setup: Install');
        // Do the tables exist? - Run uninstall first
        $tmp=$this->application->installed(array('session', 'security', 'users'), True);
        if($tmp[0] &&  isset($opt[0]) && $opt[0] == 'remove') {
            log_message('debug', 'uninstall');
            $this->_uninstall(True);
        }
        elseif($tmp[0]) {
            foreach ($tmp[1] as $value) {
                log_message('error', 'Table '.$value.' already installed');
                $this->messages->add('Table '.$value.' already installed', 'error');
            }
            $this->load->view('setup');
        }
        else {
            // If table exist - Check? skip?error?
            if(!$this->_create_session())
                show_error('Setup: Install - Failed to install session table');
            if(!$this->_create_users())
                show_error('Setup: Install - Failed to create users table');
            if(!$this->_set_user())
                show_error('Setup: Install - Failed to create user');
            if(!$this->_create_security_log())
                show_error('Setup: Install - Failed to security log');
            $data['title'] = 'Installation Complete';
            $data['message'] = '<a href="'.site_url().'/'.$GLOBALS['redirect'].'">Click here to load the application.</a>';
            $this->load->view('setup',$data);
        }
    }
    /* Upgrades currently not applicable/offered
    */
    public function upgrade()
    {
        log_message('debug', 'Installer: Upgrade');
        show_error('Not implimented');
    }
    /* Uninstall
    Uninstall application tables: By default remove all tables and folders used by the application
    */
    public function uninstall()
    {
        $ifExist = True;
        $opts = func_get_args();
        log_message('debug', 'Uninstalling Application');
        $data['messages'] = $this->_uninstall($ifExist);
        $this->load->view('setup', $data);
    }
    /* Uninstall
    Uninstall application tables: By default remove all tables and folders used by the application
    */
    private function _uninstall($ifExist)
    {
        log_message('debug','_uninstall');
        foreach($GLOBALS['tables'] as $opt)
        {
            log_message('debug', 'Uninstalling '.$opt);
            if($opt == 'session'){
                if($this->_delete_session($ifExist)) {
                    log_message('info', 'CI_sessions destroyed.');
                    $this->messages->add('CI_sessions destroyed.','success');
                }
                else
                {
                    og_message('error', 'CI_sessions doesn`t exist or there was an error.');
                    $this->messages->add('CI_sessions doesn`t exist or there was an error.','error');
                }
            }
            if($opt == 'users') {
                if($this->dbforge->drop_table('users', $ifExist)){
                    log_message('info', 'users destroyed.');
                    $this->messages->add('Users table dropped.','success');
                }
                else{
                    log_message('error', 'users table doesn`t exist or error.');
                    $this->messages->add('Users table doesn`t exist or error','error');
                }
            }
            if($opt == 'security_log') {
                if($this->dbforge->drop_table('security_log', $ifExist)) {
                    log_message('info', 'security_log destroyed.');
                    $this->messages->add('Security Log table dropped','success');
                }
                else {
                    log_message('error', 'Security Log  table doesn`t exist or error.');
                    $this->messages->add('Security Log table doesn`t exist or error','error');
                }
            }
        }
        return True;
    }
    /* Reset
    Reset the all table data
    */
    public function reset()
    {
        log_message('debug', 'Installer: Reset');
        
        foreach($GLOBALS['tables'] as $opt)
        {
            $this->db->truncate($opt);
        }
    }

    private function _create_session() {
        switch($this->config->item('sess_driver')) {
        case "database":
            // Create Sessions Table
            log_message('debug','Creating Sessions Database');
            if($this->db->exists($this->config->item('sess_save_path')))
                show_error('Sessions table already exists. Try uninstalling first.','500','Installation Error');
            $path = 'application/SQL/create_session.sql';
            $sql = read_file($path);
            if($sql) {
                if($this->db->query($sql)) {
                    log_message('debug', 'Installer: Session table created');
                    return TRUE;
                } else {
                    show_error('SQL querey failed');
                    log_message('debug', 'Installer: Error creating session table');
                    return FALSE;
                }
            }
            else {
                show_error('Unable to load install SQL script, '.$path.' doesn`t exist');
                log_message('debug', 'Installer: Error creating session table');
                return FALSE;
            }
            if(!$this->db->exists($this->config->item('sess_save_path')))
                show_error('Failed to create Sessions table','500','Installation Error');
            break;
        case 'files':
            log_message('debug','Creating folder for session info');
            $sessionpath = $this->config->item('sess_save_path');
            log_message('debug', 'Creating session storage folder: '.$sessionpath);
            if(!is_dir($sessionpath)) mkdir($sessionpath);
            return TRUE;
        break;
        default:
          log_message('error','Unsupported session driver: '+$this->config->item('sess_driver'));
          return FALSE;
        }
    }
    /* Delete everything irrespective of the configuration
    No check is made to see if it is a session table, need to verify its not going to delete protexted tables
    */
    private function _delete_session($ifExist)
    {
        // Drop Sessions Table
        log_message('debug','Delete session table');
        $db = $this->dbforge->drop_table($this->config->item('sess_save_path'), $ifExist);
        
        log_message('debug','Delete session path');
        if(is_dir($this->config->item('sess_save_path'))) {
            log_message('debug','Emptying folder, and removing folder');
            $dir1 = array_map('unlink', glob($this->config->item('sess_save_path').'/*'));
            $dir2 = rmdir($this->config->item('sess_save_path'));      
        }
        return $db or $dir;
    }
    private function _create_users()
    {
        $fields = array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '25'
                ),
            'password' => array(
                'type' =>'VARCHAR',
                'constraint' => '50'
                ),
            'name' => array(
                'type' =>'VARCHAR',
                'constraint' => '50'
                ),
            'people' => array(
                'type' =>'INT',
                'constraint' => '3',
                'unsigned' => TRUE,
                'default' => 0
                ),
            'permission' => array(
                'type' =>'INT',
                'constraint' => '3',
                'default' => -1
                ),
            'modified_date' => array(
                'type' =>'DATETIME'
                ),
            'modified_user' => array(
                'type' =>'INT',
                'constraint' => '3',
                'default' => -1
               ),
            'locked' => array(
                'type' =>'BOOL',
                'default' => 0
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('users');
        return TRUE;
    }
    private function _set_user($user=False,$pass=False,$name=False)
    {
        // Default username and password for initial account
        // Take from config
        // Randomise
        // Email?
        if(!$user) $user = 'admin';
        if(!$name) $name = 'Admin';
        if(!$pass) $pass = 'password';
        $data = array(
            'username' => $user,
            'password' => md5($pass),
            'name' => $name,
            'people' => 0,
            'permission' => 0,
            'modified_date' => date( 'Y-m-d H:i:s'),
            'modified_user' => 0
        );
        $this->db->insert('users', $data);
        return TRUE;
    }
    private function _create_security_log()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
                ),
            'ip' => array(
                'type' =>'VARCHAR',
                'constraint' => '50',
                ),
            'date' => array(
                'type' =>'DATETIME',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('security_log');
        return TRUE;
    }
    /* Install Stock Items
    */
    private function _create_stock_items()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
                ),
            'group' => array(
                'type' =>'VARCHAR',
                'constraint' => '50',
                ),
            'pack_size' => array(
                'type' =>'VARCHAR',
                'constraint' => '50',
                ),
            'pack_units' => array(
                'type' =>'int',
                'constraint' => '3',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('stock_items');
        return TRUE;
    }
    private function _create_stock_item_group()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'group' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('stock_item_group');
        return TRUE;
    }
    private function _create_stock_check_date()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'date' => array(
                'type' =>'DATETIME',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('stock_check_date');
        return TRUE;
    }
    private function _create_stock_item_inventory()
    {
        $fields = array(
            'row_id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'stock_check_date' => array(
                'type' => 'INT',
                'constraint' => '3',
                ),
            'stock_item_group' => array(
                'type' =>'INT',
                'constraint' => '3',
                ),
            'stock_items' => array(
                'type' =>'INT',
                'constraint' => '3',
                ),
            'quantity' => array(
                'type' =>'INT',
                'constraint' => '3',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('security_log');
        return TRUE;
    }
    private function _create_stock_location()
    {
        $fields = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'location' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('stock_location');
        return TRUE;
    }
    /* Show the detailed status
    */
    public function status() {
        $status = $this->application->status();
        $status['session'] = ($status['session_db'] && $status['session_driver'] == 'database') || ($status['session_path'] && $status['session_driver'] == 'files');
        $this->load->view('setup',$data);
    }
    /* Show the detailed status
    */
    public function installed() {
        $tmp = $this->application->installed();
        $data['title'] = $tmp[0];
        $data['messages'] = $tmp[1];
        $this->load->view('setup',$data);
    }
}
