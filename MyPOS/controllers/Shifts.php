<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Authenticated_Controller.php');
class Shifts extends Authenticated_Controller {

	public function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
        $this->load->model('shiftsmodel');
		$this->load->helper('url');
		$this->load->helper('table');
		$this->_init();
	}
	/* Get the staff list
	*/
	public function index() {
		$data = $this->shiftsmodel->get();
		$this->load->view('output', array('output'=>table($data)));
		/*foreach($data['list'] as $item) {
			$dif = strtotime($item->end)-strtotime($item->start);
			echo "<p>".$item->name." ".$item->start." ".$item->end." ".date("h:i",$dif)."</p>";
		}*/
	}
	public function get() {
		$this->shiftsmodel->get();
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */