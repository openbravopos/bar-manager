<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Authenticated_Controller.php');
class Settings extends Authenticated_Controller {

	public function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
        $this->load->model('resources');
		$this->_init();
	}
	/* Get the staff list
	*/
	public function index()
	{
		$this->load->helper('table');
		$data = $this->resources->index();
		$fn = function(&$row) {
			if($row->TYPE) $row->img='<img src="'.current_url().'/image/'.$row->ID.'">';
			else $row->img='<a href="'.current_url().'/content/'.$row->ID.'">x</a>';
		};
		$d=array_map($fn,$data);
		echo table($data, array('ID','NAME','img'));
	}
	public function get($key=Null)
	{
		if(is_null($key)) {
			$data['stafflist'] = $this->people->get();
			$this->load->view('staff/list',$data);
		}
		else {
			$data['staff'] = $this->people->detail($key);
			$this->load->view('staff/profile',$data);
		}
	}
	public function post()
	{

	}
	public function update($userid)
	{
		$this->people->NAME = $this->input->post('name');
		//$this->people->APPPASSWORD = ""
		$this->people->ROLE = 0;
		$this->people->VISIBLE = False;
		$this->people->update($userid);
	}
	public function delete($userid)
	{
		$this->people->delete($userid);
	}
	public function add()
	{

		$data = array(
			'title'=>'New POS User',
			'submit'=>'Add',			
			'elements'=> array(
					array(
					'label'=>'Name',
					'type'=>'text',
					'name'=>'name'
					)
				)
			);
		if($_SERVER['REQUEST_METHOD'] == 'GET')
			$this->load->view('form',$data);
		elseif($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->people->NAME = $this->input->post('name');
			$this->people->ROLE = 0;
			$this->people->VISIBLE = False;
			$this->people->create();
		}
	}
	public function image($id) {
		header('Content-Type: image/jpeg');
		echo $this->resources->content($id);
	}
	public function content($id) {
		//header('Content-Type: image/jpeg');
		$this->load->view('output', array('output'=>implode('<br />',explode('\n', json_encode($this->resources->content($id))))));
		//print_r($this->resources->content($id));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */