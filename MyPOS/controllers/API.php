<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
//require_once('Authenticated_Controller.php');
//class API extends Authenticated_Controller {
class Api extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function index() {
		$this->load->model('ticketlines');
		echo json_encode($this->ticketlines->ticketlines());
	}
	public function cash() {
		$this->load->model('cash');
		echo json_encode($this->cash->closed());
	}
}