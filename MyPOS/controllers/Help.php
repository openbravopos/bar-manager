<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Help controller for WebApp
Help Screen

Requires:
*/


require_once('Front_Controller.php');
class Help extends Front_Controller {

	public function __construct()
	{
		parent::__construct();
		//$this->security->get_logged_in_employee_info();
		$this->output->set_common_meta($this->config->item('application.name').' Help Guide', '', '');
		$this->_init();
	}

	public function index()
	{ # error, success, message
		$this->messages->add('Sample Error','error');
		$this->messages->add('Sample Message','message');
		$this->messages->add('Sample Success','success');
		$this->load->view('help');
	}
	public function page()
	{	
		$this->load->view('help');
	}
}
?>