<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		$this->path = $this->config->item('log_path') != '' ? $this->config-item('log_path') : APPPATH .'/logs/';
	}
	public function index() {
		// List log files
		$data['list'] = $this->_list();
		foreach($data['list'] as $item) {
			echo "<p><a href='".site_url()."/logs/read/".$item."'>".$item."</a></p>";
		}
	}
	public function read($filter="ERROR")
	{
		$data['filename'] = 'MyPOS/logs/log-'.date('Y-m-d').'.php';
		if(file_exists($data['filename'])) {
			$data['list'] = $this->_get($data['filename'], $filter);
			$this->load->view('list',$data);
		} else {
		echo 'Cannot find '.$data['filename'];
		}
	}
	/* List config files
	*/
	function _list() {
		$this->load->helper('directory');
		return directory_map($this->path, 1);
	}
	/* Return a config file
	*/
	function _get($filename, $filter=None) {
		//return explode(PHP_EOL, file_get_contents($filename));
		function startsWith($haystack) {
			$needle = 'ERROR';
    		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
		}
		return array_filter(file($filename),"startsWith");
	}
}
