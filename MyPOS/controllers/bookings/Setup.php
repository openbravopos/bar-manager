<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
# We require the basic unauthenticated controller
require_once(APPPATH.'controllers/Front_Controller.php');

/* MyPOS Bookings Extension Setup
 * Provide installation, update, uninstall
 * Permission: None
 * 
*/

// Tables related to this application - These are dropped or 
$tables = array('bookins');
$objects = array();
$redirect = 'bookings/dashboard';
class Setup extends Front_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();
    }
    /* Launch the application setup
    */
    public function index()
    {
        #$this->load->view('bookings/setup');
        log_message('debug', 'Setup Bookings');
        $this->load->view('output',array('output'=>'Bookings'));
        
    }

    public function info($page=False)
    {
        echo "status";
    }
    /* Install:
    Create tables: users
    Setup sessions for CI
    Setup the default user
    */
    public function install()
    {
        $opt = func_get_args();
        log_message('debug', 'Setup: Install');
        // Do the tables exist? - Run uninstall first
        $tmp=$this->_installed(array('session','security','users'), True);
        if($tmp[0] &&  isset($opt[0]) && $opt[0] == 'remove') {
            $this->_uninstall(True);
        }
        elseif($tmp[0]) {
            foreach ($tmp[1] as $value) {
                $data['messages'][] = 'Table '.$value.' already installed';
            }
            $this->load->view('setup',$data);
        }
        // If table exist - Check? skip?error?
        if(!$this->_create_session())
            show_error('Setup: Install - Failed to install session table');
        if(!$this->_create_users())
            show_error('Setup: Install - Failed to create users table');
        if(!$this->_set_user())
            show_error('Setup: Install - Failed to create user');
        if(!$this->_create_security_log())
            show_error('Setup: Install - Failed to security log');


        $data['message'] = '<p>Installation Complete</p><p><a href="'.site_url().'/'.$GLOBALS['redirect'].'">Click here to load the application.</a></p>';
        $this->load->view('setup',$data);
    }
    /* Upgrades currently not applicable/offered
    */
    public function upgrade()
    {
        log_message('debug', 'Installer: Upgrade');
        show_error('Not implimented');
    }
    /* Uninstall
    Uninstall application tables: By default remove all tables and folders used by the application
    */
    public function uninstall()
    {
        $ifExist = True;
        $opts = func_get_args();
        log_message('debug', 'Installer: Uninstall');
        $data['messages'] = $this->_uninstall($ifExist);
        $this->load->view('setup',$data);
    }
    /* Uninstall
    Uninstall application tables: By default remove all tables and folders used by the application
    */
    private function _uninstall($ifExist)
    {
        foreach($GLOBALS['tables'] as $opt)
        {

        }
        return $messages;
    }
    /* Reset
    Reset the all table data
    */
    public function reset()
    {
        log_message('debug', 'Installer: Reset');
        foreach($GLOBALS['tables'] as $opt)
        {
            $this->db->truncate($opt);
        }
    }
    /* Create the bookings table
    */
    private function _create_bookings()
    {
        $fields = array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 3,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => '25'
                ),
            'password' => array(
                'type' =>'VARCHAR',
                'constraint' => '50'
                ),
            'name' => array(
                'type' =>'VARCHAR',
                'constraint' => '50'
                ),
            'people' => array(
                'type' =>'INT',
                'constraint' => '3',
                'unsigned' => TRUE,
                'default' => 0
                ),
            'permission' => array(
                'type' =>'INT',
                'constraint' => '3',
                'default' => -1
                ),
            'modified_date' => array(
                'type' =>'DATETIME'
                ),
            'modified_user' => array(
                'type' =>'INT',
                'constraint' => '3',
                'default' => -1
               ),
            'locked' => array(
                'type' =>'BOOL',
                'default' => 0
                ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('users');
        return TRUE;
    }
 
    /* Determine the status of $name
    */
    private function _installed($names=Null, $expect=False) {
        if(is_null($names)) $names = $GLOBALS['objects'];
        $status = $this->_status();
        $status['session'] = ($status['session_db'] && $status['session_driver'] == 'database') || ($status['session_path'] && $status['session_driver'] == 'files');
        $errors = 0;
        $data=array();
        foreach($names as $name) {
            if($status[$name] == $expect) {
                $data[]=$name;
                $errors++;
            }
        }
        return array($errors, $data);
    }
    /* Get detailed status for each componenant as an Array
    */
    private function _status() {
    	return array(
    		'session_db' => $this->db->table_exists($this->config->item('sess_save_path')),
    		'session_path' => is_dir($this->config->item('sess_save_path')),
    		'session_driver' => $this->config->item('sess_driver'),
    		'_pos' => $this->db->table_exists('APPLICATIONS'),
    		'security' => $this->db->table_exists('security_log'),
            'users' => $this->db->table_exists('users')
    		);
    }
    /* Show the detailed status
    */
    public function status() {
        $status = $this->_status();
        $status['session'] = ($status['session_db'] && $status['session_driver'] == 'database') || ($status['session_path'] && $status['session_driver'] == 'files');
        $data['messages'] = $status;
        $this->load->view('setup',$data);
    }
    /* Show the detailed status
    */
    public function installed() {
        $tmp = $this->_installed();
        $data['title'] = $tmp[0];
        $data['messages'] = $tmp[1];
        $this->load->view('setup',$data);
    }
}
