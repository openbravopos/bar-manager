<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/

class Authenticated_Controller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('auth');
		$this->load->database();
		if(!$this->auth->is_logged_in()) {
			log_message('info', 'User not authenticated, redirecting');
			// Redirect away!
			redirect('welcome','location',302);
			// Mark as unathorised
			/*$response = array('status' => 'OK');
			$this->output
			        ->set_status_header(401)
			        ->set_content_type('text/html', 'UTF-8')
			        ->set_output($this->load->view('output',array('output' => 'Login'),True))
			        ->_display();
			exit;*/
		}
		//$this->security->get_logged_in_employee_info();
		$static = $this->config->item('static');
		$packages = $this->config->item('packages');
		$path = $static['path'];
		foreach($static['css'] as $css)
			$this->load->css($path.'/themes/default/css/securearea.css');
		foreach($static['js'] as $js)
			$this->load->css($path.'/themes/default/css/securearea.css');		
		foreach($static['packages'] as $pkg)
			foreach ($packages[$pkg] as $item)
				if(pathinfo($item)['extension'] == 'css')
					$this->load->css($path.$item);
				elseif(pathinfo($item)['extension'] == 'js')
					$this->load->js($path.$item);
		$this->_init();
	}

	public function _init()
	{	//$this->output->set_template('admin');
		//$this->output->set_template('sublime');
		$this->output->set_template($this->config->item('application.theme'));
		$this->load->section('nav','themes/nav');
		$social['data'] = array();
		$this->load->section('social','themes/social', $social);
		$this->load->section('header','themes/header');
		$this->load->section('footer', 'themes/footer');
		$this->load->section('banner', 'themes/banner');
		$this->load->section('user', 'login');
				$data['nav'] = array(
			'sales' => 'Closed Cash',
			'staff' => 'Staff',
			'users' => 'Users',
			'catalogue' => 'Catalogue',
			'logout' => 'Logout'
			);
		$this->load->section('pagenav','nav',$data);
	}
	public function index()
	{
		echo "";
	}
}