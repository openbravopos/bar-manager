<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once('Authenticated_Controller.php');
class Catalogue extends Authenticated_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Products');
		$this->output->set_common_meta('Products', '', '');
		$this->load->helper('table');
	}
	public function index()
	{	
		$data = table($this->Products->index(), array('Code'=>'code','Name'=>'name'), array('code'=>function($value){return '<a href="catalogue/product/'.$value.'">'.$value.'</a>';}));
		$this->load->view('output',array('output'=>$data));
	}
	public function add()
	{
		$this->load->view('output');
	}
	public function delete()
	{
		$this->load->view('output');
	}
	public function product($code)
	{
		$this->detail($code);
	}
	public function detail($code=0)
	{
		if($code==0)
			#Message Null ID
			return False;
		$data = table($this->Products->item($code));
		$this->load->view('output',array('output'=>$data));
		$this->load->view('output');
	}
	public function show()
	{
		$this->Products->index();
		$this->index();
	}
	public function hide()
	{
		$this->Products->index();
		$this->index();
	}
}
