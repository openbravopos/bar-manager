<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Authenticated_Controller.php');
class Authenticated extends Authenticated_Controller {

	public function __construct()
    {
        // Call the Model constructor
        
        parent::__construct();
		$this->_init();
	}
	public function index()
	{	
		$this->load->model('people');
		$data['stafflist'] = $this->people->get();
		$this->load->view('staff/list',$data);
	}
