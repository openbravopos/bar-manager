<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once('Dashboard.php');
class Sales extends Dashboard {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('table');
	}
	public function index($start=NULL, $end=NULL)
	{	
		$data = array();
		$this->load->model('cash');
		if(is_null($start))	$start = 0;
		elseif(is_string($start)) {
			try { $start = strtotime($start); }
			catch (Exception $e) { $this->messages->add('Caught exception: '.$e->getMessage(),'error'); }
		}
		else {
			$this->messages->add("Invalid start date",'error');
			$start = 0;
		}
		if(is_null($end))	$end = time();
		elseif(is_string($end)) {
			try { $end = strtotime($start); }
			catch (Exception $e) { $this->messages->add('Caught exception: '.$e->getMessage(),'error'); }
		}
		else {
			$this->messages->add("Invalid start date",'error');
			$end = time();
		}
		$data['sales'] = $this->cash->closed($start, $end, False) ;
		$data['total'] = $this->cash->closed($start, $end, True) ;
		//$this->load->view('output',array('output'=>json_encode($data['sales'])));
		$this->load->view('output',array('output'=>table($data['sales'],array('start','end','host','nosales','sequence','payment','total'),
			array('start'=> function($value) { return date('d-m-y H:i', strtotime($value)); },
				'total' => function($value) { return '£'.number_format($value,2);}
				))));

	}
		public function byMonth ()
	{	
		$data = array();
		$this->load->model('cash');
		$data['sales'] = $this->cash->closed();
		$this->load->view('output',array('output'=>json_encode($data['sales'])));
		
	}
	public function detail($key)
	{	
		$data = array();
		$this->load->model('cash');
		$row = $this->cash->summary($key);
		$data['total'] = money($row[0]->total);
		$data['date'] = datef($row[0]->end);
		$data['sales'] = $this->cash->detail($key);
		$this->load->view('cash/closedcashdetail',$data);
	}

	public function products($key, $start, $end)
	{
    $this->db->select('CLOSEDCASH.datestart as date, sum(TICKETLINES.price) as cash, sum(TICKETLINES.units) as units, PRODUCTS.name as product, products.code as id');
	$this->db->join('TICKETS', 'TICKETS.id = TICKETLINES.ticket');
	$this->db->join('RECEIPTS', 'TICKETS.id = RECEIPTS.id ');
	$this->db->join('CLOSEDCASH', 'CLOSEDCASH.money = RECEIPTS.money ');
	$this->db->join('PRODUCTS', 'PRODUCTS.id = TICKETLINES.product ');
	$this->db->from('TICKETLINES');
	$this->db->group_by("products.name"); 
	$this->db->group_by("CLOSEDCASH.money"); 
	$this->db->where('PRODUCTS.code <=','110');
	$this->db->where('CLOSEDCASH.datestart >=',date('Y-m-d', strtotime("01 Apr 2014")));
	$this->db->where('CLOSEDCASH.dateend <=',date('Y-m-d', strtotime("30 Sep 2014")));
	#$this->db->where('CLOSEDCASH.money',$key);	# datestart , dateend $this->db->where('id <', $id);
	$this->db->order_by('date');
	$data = $this->db->get()->result();
	print "<table>";
	foreach($data as $row){
		print "<tr><td>".$row->date."</td><td>".$row->cash."</td><td>".$row->units."</td><td>".$row->product."</td><td>".$row->id."</td></tr>";
	}
	print "</table>";
	print "Query";
	print $this->db->last_query();
	}
	#cash
	#monthly
}