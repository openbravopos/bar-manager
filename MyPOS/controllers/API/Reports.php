<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once('Authenticated_Controller.php');
class Reports extends Authenticated_Controller {

	public function __construct()
	{
		parent::__construct();
	}
}