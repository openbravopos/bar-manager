<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once(APPPATH.'/controllers/Authenticated_Controller.php');
class PeopleAPI extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('people');
	}
	public function index()
	{
		$data = $this->people->get();
$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));	
}
}
