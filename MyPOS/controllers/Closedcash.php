<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Front Controller

An implimentation of the MX->CI_Controller for an authenticated area
*/
require_once('Dashboard.php');
class Closedcash extends Dashboard {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('format');
	}
	public function index($fmt='html', $start=NULL, $end=NULL)
	{	
		$data = array();
		$this->load->model('cash');
		if(is_null($start))	$start = 0;
		elseif(is_string($start)) {
			try {
				$start = strtotime($start);
			}
			catch (Exception $e) {
    			echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			//finally {
    		//	echo "Second finally.\n";
			//}
		}
		else {
			echo "Invalid date";
			$start = 0;
		}
		
		if(is_null($end))	$end = time();
		elseif(is_string($end)) {
			try {
				$end = strtotime($start);
			}
			catch (Exception $e) {
    			echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			//finally {
    		//	echo "Second finally.\n";
			//}
		}
		else {
			echo "Invalid date";
			$end = time();
		}
		//$data['sales'] = $this->cash->takings(strtotime('2014-10-01'), strtotime("2015-01-01"), False) ;
		//$data['total'] = $this->cash->takings(strtotime('2014-10-01'), strtotime("2015-01-01"), True) ;
		$data['sales'] = $this->cash->closed($start, $end, False) ;
		$data['total'] = $this->cash->closed($start, $end, True) ;
		//log_message('DEBUG',$this->db->last_query());
		switch ($fmt) {
			case 'html':
        		//$this->load->view('cash/takings',$data);
				$d['table'] = $data['sales'];
				$d['header'] = array_keys($data['sales'][0]);
				$this->load->view('table',$d);
        		break;
    		case 'csv':
    		$this->output->unset_template();
        		$data = $this->load->view('cash/takings_csv',$data, true);
        		$this->output->set_output($data);
        		$this->output->set_content_type('text/csv');
        		header('Content-Disposition: attachment; filename="downloaded.csv"');
        		break;
		    default:
		       echo "Unkown Format";
		}
	}
	public function detail($key)
	{	
		$data = array();
		$this->load->model('cash');
		$row = $this->cash->summary($key);
		$data['total'] = money($row[0]->total);
		$data['date'] = datef($row[0]->end);
		$data['sales'] = $this->cash->detail($key);
		$this->load->view('cash/closedcashdetail',$data);
	}
}