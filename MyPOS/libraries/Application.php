<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Application {
	protected static $ci;
	/****************************
	Code Igniter Application Module
	****************************/
	private $tables = array('session','_pos','security', 'users');

	public function __construct()
	{
		self::init();
		// Need to check if the application is installed
		log_message('debug', 'Application module loaded');
	}
	
	private static function init()
	{
		/* Get access to Code Igniter
		*/
		self::$ci =& get_instance();
        try {
            self::$ci->load->database();
        } catch (Exception $e) {
            log_message('debug', 'error conneciting to database');
            show_error('Database not available');
        }
		
	}
	/* Get detailed status for each componenant as an Array
    */
    public function status() {
    	return array(
    		'session_db' => self::$ci->db->table_exists(self::$ci->config->item('sess_save_path')),
    		'session_path' => is_dir(self::$ci->config->item('sess_save_path')),
    		'session_driver' => self::$ci->config->item('sess_driver'),
    		'_pos' => self::$ci->db->table_exists('APPLICATIONS'),
    		'security' => self::$ci->db->table_exists('security_log'),
            'users' => self::$ci->db->table_exists('users')
    		);
    }
    /* Determine the status of tablename: $name
    */
    public function installed($names=Null, $expect=False) {
        if(is_null($names)) $names = $this->tables;
        $status = self::status();
        $status['session'] = ($status['session_db'] && $status['session_driver'] == 'database') || ($status['session_path'] && $status['session_driver'] == 'files');
        $errors = 0;
        $data = array();
        foreach($names as $name) {
            if($status[$name] == $expect) {
                $data[]=$name;
                $errors++;
            }
        }
        return array($errors, $data);
    }
}