<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* 
 * Author: Martyn Bristow
 * E-mail: martyn.bristow@gmail.com
*/
class Auth {
	/****************************
	Code Igniter Security Module
	****************************/

	protected static $ci;

	public function __construct()
	{
		self::init();
		// Need to check if the application is installed
		self::$ci->load->library('application');
		if(self::$ci->application->installed())
		{
			self::$ci->load->library('session');
			log_message('debug', 'Authentication module loaded');
		}
		else
		{
			log_message('debug', 'Application not installed');
			die();
		}
	}
	private static function init()
	{
		/* Get access to Code Igniter
		*/
		self::$ci =& get_instance();
	}
	public function login($username, $password)
	{
		log_message('debug', 'Login: '.$username);
		//$query = self::$ci->db->get_where('users', array('username' => $username,'password'=>md5($password)), 1);
		self::$ci->db->select('user_id, name, password, permission');
		$query = self::$ci->db->get_where('users', array('username' => $username));
		if ($query->num_rows() ==1) {
			$row = $query->row();
			if(password_verify($row->password,$password) or $row->password == md5($password)) {
			//if() {
				log_message('debug', 'Password: match');
				log_message('debug', json_encode($row));

				self::$ci->session->set_userdata('id', $row->user_id);
				//self::$ci->messages->add('Welcome back '.$row->name, 'success');
				$_SESSION['message'] = 'Welcome back '.$row->name;
				self::$ci->session->mark_as_flash('message');
				return true;
			}
			else {
				log_message('debug', 'Password: incorrect');
				self::$ci->messages->add('Invalid Password', 'error');
				# If we recieve an invalid login report it
				$row = array(
        			'ip' => self::$ci->input->ip_address(),
        			'date' => "now()"
				);
				self::$ci->db->insert('security_log', $row);
				//self::$ci->db->update('users', array('username', $username));
				return false;
			}
		}
		elseif ($query->num_rows() == 0) {
			log_message('debug', 'User not known');
			self::$ci->messages->add('Username unknown', 'error');
			return false;
		}
		else {
			log_message('error','Multiple usernames!!');
			die();
		}
		return false;
		
	}
	public function logout()
	{
		/* Logout current user
		- Debug message
		- session.destroy
		- DB::users.logout
		*/
		self::$ci->session->sess_destroy();
		log_message('debug', 'Loggout');
		return True;
	}
	/*
	Determins if a employee is logged in
	*/
	
	function is_logged_in()
	{
		return self::$ci->session->userdata('id') != false;
	}
	
	/*
	Gets information about the currently logged in employee.
	*/
	
	public function get_logged_in_employee_info()
	{
		if($this->is_logged_in())
		{
			return $this->get_info($CI->session->userdata('person_id'));
		}
		return false;
	}
	
	/*
	Determins whether the employee specified employee has access the specific module.
	*/
	
	public function has_permission($module_id,$person_id)
	{
		//if no module_id is null, allow access
		if($module_id==null)
		{
			return true;
		}
		
		$query = self::$ci->db->get_where('permissions', array('person_id' => $person_id,'module_id'=>$module_id), 1);
		return $query->num_rows() == 1;
		#return false;
	}
	
	/*
	Gets information about a particular employee
	*/
	
	public function get_info($employee_id)
	{
		self::$ci->db->from('users');	
		self::$ci->db->join('people', 'people.person_id = employees.person_id');
		self::$ci->db->where('employees.person_id',$employee_id);
		$query = self::$ci->db->get();
		
		if($query->num_rows()==1)
		{
			return $query->row();
		}
		else
		{
			//Get empty base parent object, as $employee_id is NOT an employee
			$person_obj=parent::get_info(-1);
			
			//Get all the fields from employee table
			$fields = self::$ci->db->list_fields('employees');
			
			//append those fields to base parent object, we we have a complete empty object
			foreach ($fields as $field)
			{
				$person_obj->$field='';
			}
			
			return $person_obj;
		}
	}
}