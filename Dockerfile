# MyPOS Application
#
# User: admin
# Password: mysql
#
# Build:
# docker build -t="mypos" .
#
# Deploy:
# docker run -d mypos
FROM codeigniter
MAINTAINER Martyn Bristow version: 0.1

ADD docker_index.php /var/www/html/index.php
ADD MyPOS/ /var/www/html/MyPOS
RUN echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php
RUN chmod -R 755 /var/www/html
ADD assets /var/www/html/assets
