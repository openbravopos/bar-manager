# MyPOS - Web Management of Unicenta #

A CodeIgniter web application for UnicentaOPOS (and variants)

## Supports ##

 - Database: MySQL
 - POS Application: UnicentaOPOS

Drivers will be added for SQLite & Postgres databases, and Chromis and OpenBravo POS systems.


### License ###
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Copyright ###
### Testing ###
### Features ###

## Deployment ##

### Using Docker ###
#### Docker Compose ####

From within the root of the project folder, which contains the docker-compose.yaml file, run `docker-compose up -d`

#### Docker Linking ####

Windows
Linux/MacOSX
Cloud
Docker
./configure
## Upgrading ##
