#!/usr/bin/env python
""" Convert all wiki pages into html
"""

# Config
# Paths configured local to the base directory
BUILD_DIR = "_build"
WIKI_DIR = "wiki"

import os
import logging

logging.basicConfig(level=logging.DEBUG)

CSS_INCLUDE='<link href="markdown.css" rel="stylesheet"></link>'
def install(package):
    """ Install a package through pip
    """
    try:
        import pip
        pip.main(['install', package, "--user"])
    except ImportError:
        print "No PIP available, please install pip "

def list_wiki_pages(path=WIKI_DIR):
    """ Get all wiki pages
    """
    path = os.path.expanduser(WIKI_DIR)
    is_md = lambda filename: len(filename.split(".")) > 1 and filename.split(".") and filename.split(".")[-1] == "md"

    try:
        ls = os.listdir(path)
        folders = []
        files = filter(is_md, ls)
        files = map(lambda filename: os.path.join(WIKI_DIR, filename), files)
        return files
    except OSError:
        print "Error"


if __name__ == "__main__":
    try:
        import markdown
    except ImportError:
        print "Couldn't import Markdown, installing via pip"
        install('markdown')
        try:
            import markdown
        except ImportError:
            print "Failed to import markdown again, giving up!"
            exit()
    logging.info("Fetching pages")
    for page in list_wiki_pages():
        logging.info("converting %s", page)
        with open(page) as f:
            md_content = f.read()
            md_content = CSS_INCLUDE + md_content
        html_filename = "%s.html" % page.rsplit('.',1)[0]
        logging.info('Writeing to %s', html_filename)
        with open(html_filename,'w') as f:
            f.write(markdown.markdown(md_content))