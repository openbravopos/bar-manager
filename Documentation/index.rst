.. MyPOS documentation master file, created by
   sphinx-quickstart on Mon Dec 14 00:55:38 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyPOS's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

.. code-block:: php

    use myNamespace/MyClass;
    ...

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

